��    %      D              l     m     {     �     �     �     �     �     �     �  "   �       �   #  6     !   ?     a     f     v     �     �  "   �     �     �  	   �     �       "     "   /     R     X  "   j  "   �  +   �  !   �  "   �     !     A  �  a     �          '     /     7     E     S     X     a  +   m     �  �   �  ;   �	  (   �	     
     
     
     3
  "   H
  %   k
     �
     �
     �
     �
     �
     �
     �
             4     3   N  :   �  2   �  5   �     &     F   Add News Item Add news item Cancel Content Content of the news Content of the news item Delete Edit Expiration Date Expiration date can not be in past Expiration date is in the past Here you will find news relevant to users of the Energy Data Service. This could be news about events, datasets or how data has been applied. You will also find news about features in Energi Data Service, and about new versions. Incorrect expiration date format, should be YYYY-MM-DD Incorrect type, should be boolean News News item title News not found News was removed successfully. No content provided. No news are created in the system. No news found. Photo Read mode Recent News Save The news was created successfully. The news was updated successfully. Title URL to image file User not authorized to create news User not authorized to delete news User not authorized to manage subscriptions User not authorized to patch news User not authorized to update news {actor} created the news {news} {actor} updated the news {news} Project-Id-Version: ckanext-edsnews 0.0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-06-07 12:06+0200
PO-Revision-Date: 2017-05-18 11:27+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: da_DK
Language-Team: da_DK <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Tilføj nyhedselement Tilføj nyhedselement Fortryd Indhold Nyhedsindhold Nyhedsindhold Slet Redigér Udløbsdato Udløbsdatoen kan ikke være før dags dato Udløbsdato er overskredet Her finder du nyheder, som er relevant for brugere af Energidataservice. Dette kan være nyheder om events, datasæt eller hvordan data er blevet tilføjet. Du kan også finde nyheder om features i Energidataservice og om nye versioner. Udløbsdato er ikke angivet korrekt. Formatet er YYYY-MM-DD Type er ikke korrekt, skal være boolean Nyheder Nyhedstitel Nyheden blev ikke fundet Nyheden blev fjernet Der er intet indhold tilgængeligt Ingen nyheder er oprettet i systemet. Ingen nyheder blev fundet Foto Læsetilstand Seneste nyheder Gem Nyheden blev oprettet. Nyheden blev opdateret Titel URL til billedefil Brugeren har ikke rettigheder til at oprette nyheder Brugeren har ikke rettigehder til at slette nyheder Brugeren har ikke adgang til at administrere medlemsskaber Brugeren har ikke rettigheder til at rette nyheder Brugeren har ikke rettigheder til at opdatere nyheder {actor} created the news {news} {actor} updated the news {news} 