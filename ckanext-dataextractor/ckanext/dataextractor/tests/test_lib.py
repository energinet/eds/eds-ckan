# -*- coding: utf-8 -*-
from ckanext.dataextractor.lib.file_writer_service import FileWriterService


def test_csv_writer():
    writer = FileWriterService()
    # First test with ascii only:
    stream = writer.write_to_file(fields=[{'id':'a'},{'id':'b'}],
                                  records=[{'a':1,'b':2},{'a':3,'b':4}],
                                  format='csv',
                                  delimiter=',',
                                  metadata=None)
    result = stream.getvalue()
    assert result[0] == 'a'
    # Now test with utf8 char:
    stream = writer.write_to_file(fields=[{'id': u'й'},{'id': u'щ'}],
                                  records=[{u'й':1,u'щ': u'ы'},{u'й':3,u'щ':4}],
                                  format='csv',
                                  delimiter=',',
                                  metadata=None)
    expected = u'й,щ\r\n1,ы\r\n3,4\r\n'
    result = stream.getvalue()
    result = result.decode('utf-8')
    assert result == expected

def test_xlsx_writer():
    writer = FileWriterService()
    # First test with ascii only:
    stream = writer.write_to_file(fields=[{'id':'a'},{'id':'b'}],
                                  records=[{'a':1,'b':u'ш'},{'a':3,'b':4}],
                                  format='xlsx',
                                  delimiter=None,
                                  metadata={'attributes': [{'type': ''}]})
    result = stream.getvalue()
    assert result
    # Now test with utf8 char:
    stream = writer.write_to_file(fields=[{'id': u'й'},{'id': u'щ'}],
                                  records=[{u'й':1,u'щ': u'ы'},{u'й':3,u'щ':4}],
                                  format='xlsx',
                                  delimiter=None,
                                  metadata={'attributes': [{'type': ''}]})
    expected = u'й,щ\r\n1,ы\r\n3,4\r\n'
    result = stream.getvalue()
    assert result
    # Now test with NaN value:
    stream = writer.write_to_file(fields=[{'id':'a'},{'id':'b'}],
                                  records=[{'a':1,'b':u'ш'},{'a':3,'b':float('NaN')}],
                                  format='xlsx',
                                  delimiter=None,
                                  metadata={'attributes': [{'type': ''}]})
    result = stream.getvalue()
    assert result
