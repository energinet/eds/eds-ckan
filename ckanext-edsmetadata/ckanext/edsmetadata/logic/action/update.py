# -*- coding: utf-8 -
import logging
import ckan.logic as l
import json
from ckanext.edsmetadata.logic.validators import validate_resource_attributes_list

import ckanext.edsmetadata.helpers as h

log = logging.getLogger(__name__)


def package_update(context, data_dict):
    if 'resources' in data_dict and any(data_dict['resources']):
        resources = data_dict.pop('resources')
        for res in resources:
            if 'schema' in res:
                if isinstance(res['schema'], dict):
                    res['schema'] = json.dumps(res['schema'])
            if 'filters' in res:
                if isinstance(res['filters'], list):
                    res['filters'] = json.dumps(res['filters'])
            elif 'filters' not in res and 'id' in res:
                res['filters'] = h._resource_extra_obj(res['id'], key='filters')
            else:
                res['filters'] = '[]'

            if 'attributes' in res:
                if isinstance(res['attributes'], list):
                    res['attributes'] = json.dumps(res['attributes'])
                elif isinstance(res['attributes'], basestring):
                    pass
                else:
                    res['attributes'] = json.dumps([])
            else:
                res['attributes'] = json.dumps([])
        data_dict['resources'] = resources

    return l.action.update.package_update(context, data_dict)


def resource_update(context, data_dict):
    data_dict['attributes'] = validate_resource_attributes_list(data_dict.get('attributes', []))
    data_dict['schema'] = validate_resource_attributes_list(data_dict['schema'])

    return l.action.update.resource_update(context, data_dict)
