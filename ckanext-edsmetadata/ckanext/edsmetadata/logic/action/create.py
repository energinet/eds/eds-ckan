# -*- coding: utf-8 -
import logging
import ckan.logic as l
from ckanext.edsmetadata.logic.validators import validate_resource_attributes_list

log = logging.getLogger(__name__)


def resource_create(context, data_dict):
    data_dict['attributes'] = validate_resource_attributes_list(data_dict.get('attributes', []))
    data_dict['schema'] = validate_resource_attributes_list(data_dict['schema'])

    return l.action.create.resource_create(context, data_dict)
