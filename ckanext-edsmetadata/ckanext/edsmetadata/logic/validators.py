import logging
import json

try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

from ckan.common import _ as _trans

log = logging.getLogger(__name__)


def validate_resource_attributes_list(attributes):
    while isinstance(attributes, (unicode, basestring)):
        attributes = json.loads(attributes)

    return attributes
