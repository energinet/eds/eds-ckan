FROM eds001containerregistry-eds001.azurecr.io/ckan:2.8.1 AS builder

MAINTAINER Viderum <services@viderum.com>

USER root
# Install libxml2
RUN apk add --no-cache libxml2 \
        libxslt \
        libstdc++ \
        libxml2-dev \
        libxslt-dev \
        openssl-dev \
        build-base \
        python-dev \
        libffi-dev \
        bash

### Copy private extensions ###
COPY ckanext-eds ${APP_DIR}/src/ckanext-eds
COPY ckanext-dataextractor ${APP_DIR}/src/ckanext-dataextractor
COPY ckanext-password ${APP_DIR}/src/ckanext-password
COPY ckanext-dataguide ${APP_DIR}/src/ckanext-dataguide
COPY ckanext-edsmetadata ${APP_DIR}/src/ckanext-edsmetadata
COPY ckanext-edsnews ${APP_DIR}/src/ckanext-edsnews
COPY ckanext-googleanalytics ${APP_DIR}/src/ckanext-googleanalytics
COPY ckanext-sentry ${APP_DIR}/src/ckanext-sentry

### Install CKAN extensions ###
RUN pip install --upgrade pip && \
    pip install --user gunicorn gevent && \
    pip install --user idna && \
    pip install --user -e git+https://github.com/viderumglobal/ckanext-c3charts.git@custom-sql-expressions-ckan-v2.8#egg=ckanext-c3charts && \
    pip install --user -e git+https://github.com/ckan/ckanext-disqus.git@709566b439df6a9cf45708c773c18a71b141f3ef#egg=ckanext-disqus && \
    pip install --user -e ${APP_DIR}/src/ckanext-googleanalytics && \
    pip install --user -r ${APP_DIR}/src/ckanext-googleanalytics/requirements.txt && \
    pip install --user -e ${APP_DIR}/src/ckanext-sentry && \
    pip install --user -r ${APP_DIR}/src/ckanext-sentry/requirements.txt && \
    pip install --user -e ${APP_DIR}/src/ckanext-eds && \
    pip install --user -e ${APP_DIR}/src/ckanext-dataextractor && \
    pip install --user -r ${APP_DIR}/src/ckanext-dataextractor/requirements.txt && \
    pip install --user -e ${APP_DIR}/src/ckanext-password && \
    pip install --user -e ${APP_DIR}/src/ckanext-dataguide && \
    pip install --user -e ${APP_DIR}/src/ckanext-edsmetadata && \
    pip install --user -e ${APP_DIR}/src/ckanext-edsnews && \
    msgcat --use-first /${APP_DIR}/src/ckan/ckan/i18n/da_DK/LC_MESSAGES/ckan.po /${APP_DIR}/src/ckanext-eds/ckanext/eds/i18n/da_DK/LC_MESSAGES/ckanext-eds.po /${APP_DIR}/src/ckanext-dataextractor/ckanext/dataextractor/i18n/da_DK/LC_MESSAGES/ckanext-dataextractor.po /${APP_DIR}/src/ckanext-password/i18n/da_DK/LC_MESSAGES/ckanext-password.po /${APP_DIR}/src/ckanext-dataguide/ckanext/dataguide/i18n/da_DK/LC_MESSAGES/ckanext-dataguide.po /${APP_DIR}/src/ckanext-edsmetadata/ckanext/edsmetadata/i18n/da_DK/LC_MESSAGES/ckanext-edsmetadata.po /${APP_DIR}/src/ckanext-edsnews/ckanext/edsnews/i18n/da_DK/LC_MESSAGES/ckanext-edsnews.po -o /${APP_DIR}/src/ckan/ckan/i18n/da_DK/LC_MESSAGES/ckan.po && \
    msgfmt /${APP_DIR}/src/ckan/ckan/i18n/da_DK/LC_MESSAGES/ckan.po -o /${APP_DIR}/src/ckan/ckan/i18n/da_DK/LC_MESSAGES/ckan.mo

FROM eds001containerregistry-eds001.azurecr.io/ckan:2.8.1
WORKDIR ${APP_DIR}

MAINTAINER Viderum <services@viderum.com>

USER root

RUN pip install --upgrade pip
RUN apk add --no-cache bash py-cffi

ENV CKAN__PLUGINS datastore c3charts dataextractor dataguide custom_password_criteria edsmetadata edsnews eds disqus envvars googleanalytics sentry
ENV RELEASE_VERSION=$RELEASE_VERSION

COPY --from=builder ${APP_DIR} .
COPY --from=builder /root/.local /root/.local

ENV PATH=/root/.local/bin:$PATH

# Load envvars plugin on ini file
RUN paster --plugin=ckan config-tool ${APP_DIR}/production.ini "ckan.plugins = ${CKAN__PLUGINS}"

# Recycle database pool or the connection will timeout on Postgresql managed service.
RUN paster --plugin=ckan config-tool ${APP_DIR}/production.ini "sqlalchemy.pool_recycle = 1000"

# Add configs for limitation of login attempts.
# Add placeholder "googleanalytics.id" variable that must be updated in production.
RUN paster --plugin=ckan config-tool ${APP_DIR}/production.ini "lock_timeout = 60" && \
    paster --plugin=ckan config-tool ${APP_DIR}/production.ini "login_max_count = 3" && \
    paster --plugin=ckan config-tool ${APP_DIR}/production.ini "googleanalytics.id = 1234"

# Modify who.ini so that we use custom authenticator.
RUN sed -i 's/ckan.lib.authenticator:UsernamePasswordAuthenticator/ckanext.password.authenticator:CKANLoginThrottle/g' ${APP_DIR}/who.ini

# Dirty username fix for Postgresql as a service on Azure.
RUN sed -i 's/import copy/import copy\nfrom string import split/;s/read_connection_user = sa_url.make_url(self.read_url).username/read_connection_user = split(sa_url.make_url(self.read_url).username, "@")[0]/' /srv/app/src/ckan/ckanext/datastore/backend/postgres.py

COPY setup ${APP_DIR}

EXPOSE 5000

CMD ["/srv/app/start_ckan.sh"]
