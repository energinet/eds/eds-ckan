import logging

import ckan.plugins as p
import ckan.logic as l
import ckan.plugins.toolkit as t
import ckan.lib.navl.dictization_functions as df
from ckanext.eds.model.user_extra import UserExtra
from ckanext.eds.model.user_roles import UserRoles
from ckanext.eds.logic.schema import user_extra_delete_schema, user_roles_delete_schema

log = logging.getLogger(__name__)


def user_extra_delete(context, data_dict):
    '''Delete user extra parameter.

        :param key: Key of the parameter you want to delete.
        :type key: string

        '''

    l.check_access('user_extra', context, data_dict)
    data, errors = df.validate(data_dict, user_extra_delete_schema(),
                               context)

    if errors:
        raise t.ValidationError(errors)

    m = context.get('model')
    user_obj = m.User.get(context.get('user'))
    user_id = user_obj.id
    key = data.get('key')
    UserExtra.delete(user_id, key)

    return {
        'message': 'Extra with key: %s, deleted' % key
    }

def user_roles_delete(context, data_dict):
    '''Delete user roles parameter.

        :param user_id: id of the user you want to remove roles for.
        :type user_id: string

        '''
    l.check_access('user_roles', context, data_dict)
    data, errors = df.validate(data_dict, user_roles_delete_schema(),
                                context)

    if error:
        raise t.ValidationError(errors)

    m = context.get('model')
    user_obj = m.User.get(data.get('user_id'))

    if user_obj is None:
        l.NotFound("No such user")

    user_id = user_obj.id
    UserExtra.delete(user_id)

    return {
        'message': 'User\'s (id=%s) role deleted' % user_id
    }

def datastore_junk_delete(context, data_dict):
    l.check_access('datastore_delete', context)

    resource_found = False
    try:
        res = l.get_action('resource_show')(context, {'id': data_dict['resource_id']})
        resource_found = True
    except l.NotFound, e:
        log.info('resource {0} not found so deleting it from datastore'.format(data_dict['resource_id']))

    if resource_found:
        return {'message': 'resource exists in the CKAN database'}
    else:
        l.get_action('datastore_delete')(context, {'resource_id': data_dict['resource_id'], 'force': True})
        return {'message': 'deleted {0} from datastore'.format(data_dict['resource_id'])}

def custom_dataset_purge(context, data_dict):
    '''Delete table from the datastore before purging a dataset.'''
    l.check_access('dataset_purge', context, data_dict)

    # Get the package
    package_id = data_dict['id']
    package = t.get_action('package_show')(context, {'id': package_id})

    # Delete table from the datastore
    for resource in package['resources']:
        body = {'resource_id': resource['id']}
        try:
            response = t.get_action('datastore_delete')(context, body)
            log.info('Datastore table was deleted: {0}'.format(response['resource_id']))
        except l.NotFound, e:
            log.info('Resource {0} was not found. Now purging the package.'.format(body['resource_id']))

    # Now purge the dataset
    out = {}
    try:
        out = t.get_action('dataset_purge')(context, data_dict)
        log.info('Purged dataset: {0}'.format(package_id))
    except (l.ValidationError) as e:
        raise e

    return out
