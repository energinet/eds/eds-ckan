import logging

import ckan.plugins as p
import ckan.logic as l
from ckan.logic import ValidationError
import ckan.model as model
import ckan.plugins.toolkit as t
import ckan.lib.helpers as h
from ckan.common import _
from ckan.lib.mailer import mail_user
import ckan.lib.navl.dictization_functions as df
from ckanext.datastore.backend import InvalidDataError
from ckanext.eds.model.user_extra import UserExtra
from ckanext.eds.model.user_roles import UserRoles
from ckanext.eds.logic.schema import user_extra_schema, user_roles_schema
from ckanext.eds.logic.dictization import table_dictize
from ckanext.eds.helpers import generate_confirmation_email

log = logging.getLogger(__name__)

def user_create_custom(context, data_dict):
    # First check if all supplied information by user is valid:
    # Note that in 'user_create_validate' action, the core 'user_create' action
    # will be called.
    user_dict = l.get_action('user_create_validate')(context, data_dict)
    # Now check if user wants to subscribe to news and if so create news
    # subscription:
    if data_dict.get('subscriptiontonews'):
        try:
            subscribe_to_news = l.get_action('news_subscription_create')(
                context={'user': data_dict['name'], 'ignore_auth': True},
                data_dict={'notify_by_mail': True})
        except Exception, e:
            log.debug(e)
    # Now we want to set user's state as 'awaiting_confirmation'. We change it
    # to 'active' once user confirms email address:
    user_obj = model.User.get(data_dict['name'])
    user_obj.state = 'awaiting_confirmation'
    user_obj.save()
    # Send a confirmation email to user:
    confirmation_email = generate_confirmation_email(user_obj.id, user_obj.name)
    try:
        mail_user(user_obj, confirmation_email['subject'],
                confirmation_email['body'])
        h.flash_success(_(u'User "%s" is now registered. We have sent you a '
                      u'confirmation email. You need to confirm your email address '
                      u'before you can use the portal.') % (data_dict[u'name']))
    except Exception, e:
        log.debug(e)
        h.flash_error(str(e))
    return user_dict

def user_extra_create(context, data_dict):
    '''Create user extra parameter.

        :param key: Key of the parameter.
        :type key: string

        :param value: Value of the parameter.
        :type value: string

        :param active: State of the parameter. Default is active.
        :type active: string

        '''

    l.check_access('user_extra', context, data_dict)
    data, errors = df.validate(data_dict, user_extra_schema(),
                               context)

    if errors:
        raise t.ValidationError(errors)

    m = context.get('model')
    user_obj = m.User.get(context.get('user'))


    user_id = user_obj.id
    key = data.get('key')
    value = data.get('value')
    state = data.get('state', 'active')

    user_extra = UserExtra.get(user_id, key)
    if user_extra:
        user_extra.key = key
        user_extra.value = value
        user_extra.save()
    else:
        user_extra = UserExtra(
            user_id=user_id,
            key=key,
            value=value,
            state=state
        )
        user_extra.save()

    out = table_dictize(user_extra, context)

    return out

def user_roles_create(context, data_dict):
    l.check_access('user_roles', context, data_dict)
    data, errors = df.validate(data_dict, user_roles_schema(),
                                context)

    if errors:
        raise t.ValidationError(errors)

    m = context.get('model')
    user_obj = m.User.get(data.get('user_id'))

    if user_obj is None:
        raise l.NotFound("No such user")

    user_id = user_obj.id
    role = data.get('role')

    user_roles = UserRoles.get(user_id)
    if user_roles:
        user_roles.role = role
        user_roles.save()
    else:
        user_roles = UserRoles(
            user_id=user_id,
            role=role
        )
        user_roles.save()

    out = table_dictize(user_roles, context)

    return out

def _update_last_modified(context, resource_id):
    from datetime import datetime
    try:
        l.get_action('resource_patch')(
            context,
            {
                'id': resource_id,
                'last_modified': datetime.utcnow()
            })
    except:
        log.info('Cannot update last_modified field for resource: {0}'.format(resource_id))

def custom_datastore_create(context, data_dict):
    ''' Create custom datastore_create action that adds validation on data '''

    return _check_data_validity(context, data_dict, 'datastore_create')


def custom_datastore_upsert(context, data_dict):
    ''' Create custom datastore_upsert action that adds validation on data '''

    return _check_data_validity(context, data_dict, 'datastore_upsert')


def _check_data_validity(context, data_dict, action):
    l.check_access(action, context, data_dict)
    resource_id = data_dict.get('resource_id', '')
    out = {}
    try:
        out = l.get_action(action)(context, data_dict)
    except (ValidationError, InvalidDataError) as e:
        raise e
    else:
        _update_last_modified(context, resource_id)

    return out
