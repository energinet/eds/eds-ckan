import vdm.sqlalchemy
from vdm.sqlalchemy.base import SQLAlchemySession

from datetime import datetime

import ckan.model.meta as meta
from ckan.model.package import Package
from ckan.model.tag import PackageTag
from ckan.model.resource import Resource
from ckan.model.package_extra import PackageExtra
from ckan.model.group import (
    Member,
    Group
)
from ckan.model.group_extra import GroupExtra
from ckan.model.system_info import SystemInfo


class RepositoryEds(vdm.sqlalchemy.Repository):

    def purge_revision(self, revision, leave_record=False):
        '''Purge revisions.'''

        SQLAlchemySession.setattr(self.session, 'revisioning_disabled', True)
        self.session.autoflush = False
        # o = Package, Member ...
        purge_revision = True
        for o in self.versioned_objects:
            # revobj = MemberRevision
            revobj = o.__revision_class__
            # a list of MemberRevision with this revision id
            items = self.session.query(revobj). \
                filter_by(revision=revision).all()
            for item in items:
                continuity = item.continuity
                if continuity.revision == revision:
                     # skip removing this revision object and this revision
                     purge_revision = False
                else:
                     self.session.delete(item)

        if leave_record and purge_revision:
            revision.message = u'PURGED: %s' % datetime.now()
        elif purge_revision:
            self.session.delete(revision)
        self.commit_and_remove()
        if purge_revision:
            return {'purged': True}
        return {'purged': False}





repo_eds = RepositoryEds(meta.metadata, meta.Session,
                  versioned_objects=[Package, PackageTag, Resource,
                                     PackageExtra, Member,
                                     Group, GroupExtra, SystemInfo]
                  )
