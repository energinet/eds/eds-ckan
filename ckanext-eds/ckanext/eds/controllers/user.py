import logging

try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

import ckan.logic as logic
import ckan.lib.base as base
import ckan.model as model
import ckan.lib.mailer as mailer
import ckan.lib.helpers as h
import ckan.lib.navl.dictization_functions as df

from ckan.controllers.user import UserController
from ckan.common import _, c, request
from ckan.lib.base import render_jinja2
from ckan.lib.mailer import get_reset_link, create_reset_key, mail_user
from ckanext.eds.helpers import config_option_show, generate_confirmation_email, get_md5_hash

render = base.render
abort = base.abort

NotFound = logic.NotFound
NotAuthorized = logic.NotAuthorized
ValidationError = logic.ValidationError
check_access = logic.check_access
get_action = logic.get_action
tuplize_dict = logic.tuplize_dict
clean_dict = logic.clean_dict
parse_params = logic.parse_params

DataError = df.DataError

log = logging.getLogger(__name__)

CTRL = 'ckanext.eds.controllers.user:EDSUserController'


def _get_reset_link_body(user):
    extra_vars = {
        'reset_link': get_reset_link(user),
        'site_title_dk': config_option_show('ckan.site_title', 'da_DK'),
        'site_title_en': config_option_show('ckan.site_title', 'en'),
        'site_url': config.get('ckan.site_url'),
        'user_name': user.name,
    }
    return render_jinja2('emails/reset_password.txt', extra_vars)


def _send_reset_link(user):
    create_reset_key(user)
    body = _get_reset_link_body(user)
    extra_vars = {
        'site_title_dk': config_option_show('ckan.site_title', 'da_DK'),
        'site_title_en': config_option_show('ckan.site_title', 'en')
    }
    subject = render_jinja2('emails/reset_password_subject.txt', extra_vars)
    subject = subject.split('\n')[0]
    mail_user(user, subject, body)


class EDSUserController(UserController):
    def user_delete_custom(self):
        context = {'model': model, 'session': model.Session, 'user': c.user,
                   'auth_user_obj': c.userobj}
        data_dict = {'id': request.params.get('id')}
        try:
            check_access('user_delete', context, data_dict)
        except NotAuthorized:
            abort(403, _('Unauthorized to perform this action.'))
        try:
            subscription_delete = get_action('news_subscription_delete')(context, data_dict)
            log.info(u'{} for {}'.format(subscription_delete['message'], data_dict['id']))
        except Exception, e:
            log.info(u'News subscription doesnt exist for {}'.format(data_dict['id']) + str(e))
        # Now delete the user:
        get_action('user_delete')(context, data_dict)
        log.info(u'Following user is deleted {}'.format(data_dict['id']))
        h.flash_success('Your account has been succesfully deleted.')
        url = h.url_for(controller='user', action='logged_out_page')
        h.redirect_to(self._get_repoze_handler('logout_handler_path') +
                      '?came_from=' + url, parse_url=True)

    def confirm_email(self):
        # Check if provided token is valid and confirm user's email address
        username = request.params.get('username')
        user_obj = model.User.get(username)
        token = request.params.get('token')
        expected_token = get_md5_hash(user_obj.id)
        if token == expected_token: # Valid token - email is confirmed
            # Set user state as 'active':
            user_obj.activate()
            user_obj.save()
            # log the user in programatically and redirect to dashboard:
            h.flash_success('Your email address is succesfully confirmed!')
            return h.redirect_to(u'user.me')
        else: # invalid token, user still needs to confirm email
            h.flash_error('Invalid token, please, confirm your email address.')
            return h.redirect_to('/user/confirm_email_request?username={0}'.format(username))


    def confirm_email_request(self):
        username = request.params.get('username')
        if username:
            # Check if user is active, if not send a confirmation email
            user_obj = model.User.get(username)
            if user_obj.state == 'awaiting_confirmation':
                confirmation_email = generate_confirmation_email(user_obj.id, user_obj.name)
                try:
                    mail_user(user_obj, confirmation_email['subject'],
                            confirmation_email['body'])
                    h.flash_success(_(u'We have sent you a confirmation email. '
                                  u'You need to confirm your email address '
                                  u'before you can use the portal.'))
                except Exception, e:
                    log.debug(e)
                    h.flash_error(str(e))
            else:
                h.flash_success('Your email address is already confirmed.')
            return h.redirect_to(u'home.index')
        else:
            log.debug('Username param is required for "/user/confirm_email_request" path.')
            return abort(404, 'Not found')


    def request_reset(self):
        context = {'model': model, 'session': model.Session, 'user': c.user,
                   'auth_user_obj': c.userobj}
        data_dict = {'id': request.params.get('user')}
        try:
            check_access('request_reset', context)
        except NotAuthorized:
            abort(403, _('Unauthorized to request reset password.'))

        if request.method == 'POST':
            id = request.params.get('user')

            context = {'model': model,
                       'user': c.user}

            data_dict = {'id': id}
            user_obj = None
            try:
                user_dict = get_action('user_show')(context, data_dict)
                user_obj = context['user_obj']
            except NotFound:
                # Try searching the user
                del data_dict['id']
                data_dict['q'] = id

                if id and len(id) > 2:
                    user_list = get_action('user_list')(context, data_dict)
                    if len(user_list) == 1:
                        # This is ugly, but we need the user object for the
                        # mailer,
                        # and user_list does not return them
                        del data_dict['q']
                        data_dict['id'] = user_list[0]['id']
                        user_dict = get_action('user_show')(context, data_dict)
                        user_obj = context['user_obj']
                    elif len(user_list) > 1:
                        h.flash_error(_('"%s" matched several users') % (id))
                    else:
                        h.flash_error(_('No such user: %s') % id)
                else:
                    h.flash_error(_('No such user: %s') % id)

            if user_obj:
                try:
                    _send_reset_link(user_obj)
                    h.flash_success(_('Please check your inbox for '
                                      'a reset code.'))
                    h.redirect_to('/')
                except mailer.MailerException, e:
                    h.flash_error(_('Could not send reset link: %s') %
                                  unicode(e))
        return render('user/request_reset.html')
