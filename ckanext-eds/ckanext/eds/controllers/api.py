try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

from ckan.controllers.api import ApiController
import ckan.lib.helpers as h
import sqlalchemy


class EdsApiController(ApiController):
    ctrl = 'ckanext.eds.controllers.api:EdsApiController'
    def action(self, logic_function='datastore_search_sql', ver=3):
        try:
            return ApiController.action(self, logic_function, ver)
        except sqlalchemy.exc.DataError as e:
            return_dict = {'help': h.url_for(controller='api',
                                         action='action',
                                         logic_function='help_show',
                                         ver=ver,
                                         name=logic_function,
                                         qualified=True,
                                         )
                        }
            return_dict['error'] = {'__type': 'Integrity Error',
                                    'message': str(e)}
            return_dict['success'] = False
            return self._finish(400, return_dict, content_type='json')
