try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

from ckan.controllers.user import UserController
from ckan.views.user import index
import ckan.lib.base as base
from ckan.common import _, c


class UserListController(UserController):
    ctrl = 'ckanext.eds.controllers.user_list:UserListController'
    def user_list(self):
        if c.userobj and c.userobj.sysadmin:
            return index()
        base.abort(403, _('You\'re not authorized to view this page.'))
