###![Energi Data Service logo](media/eds-logo.png =55x30)Energy Data Service (EDS)
----------------------------
__Note__: Commits to remotes/origin/{master,develop} triggers CI builds and CI releases. Develop is automatically deployed to the development environment and master eventually triggers the release deployment process.

`eds-ckan` is the main project behind [Energi Data Service (EDS)](https://energidataservice.dk). 

The project builds a container image of the different components needed to run a CKAN instance, including the different extensions needed.

The version number in `eds-ckan` is an internal version number, used to track our EDS releases. It is, in no way, indicative the version of the underlying CKAN software.

###Automatic Build & Release
---------------------------

As mentioned earlier, this repository is used as a reference for our CI/CD system. Please keep in mind that commits to remotes/origin{master,develop} will trigger a build and release procedure.

The build procedures will attempt to build a Docker image using the included [Dockerfile](Dockerfile). Each successfully build image is tagged with
the version number, branch and build id - An example of which could be `1.6.2-develop.52464`.
